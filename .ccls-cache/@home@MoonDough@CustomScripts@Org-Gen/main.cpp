#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cctype>

#include "fileType.h"
//#include "fileType.cpp"
using namespace std;

string PATH =  "/home/MoonDough/CustomScripts/Org-Gen/Templates/";

enum Type {
  org,
  pdf,
  html,
  babel
};

string TypeStr(Type xType){
  switch(xType){
  case org:
    return "org";
  case pdf:
    return "pdf";
  case html:
    return "html";
  case babel:
    return "babel";
  default:
    cout << "ERROR: \n\tNeed to update file types cases.\n";
    break;
  }
  return "ERROR";
};

Type TypeInt(int xInt){
  switch(xInt){
  case 0:
    return org;
  case 1:
    return pdf;
  case 2:
    return html;
  case 3:
    return babel;
  default:
    cout << "ERROR: \n\tNeed to update file types cases.\n";
    break;
  }
  return org;
};

int main (){
  ifstream Template;
  // ofstream OrgFile;
  ostringstream file, cmd;
  string title, description, line, tempStr;
  char* CMD;
  int choice = -1, tempInt;
  size_t temp;

  do{
    cout << "Export type:\n";
    for(int i=0;i<sizeof(Type);i++){
      cout << "\t(" << i << ") -> " << TypeStr(TypeInt(i)) << endl;
    }

    cout << "Enter Choice: ";
    getline(cin, line);
    if(!isdigit(line[0])){
      cout << "Invalid Input.\n";
    }
    else{
      choice = stoi(line, &temp);
    }
    if(choice < 0 || choice > (sizeof(Type)-1)){
      cout << "Invalid Input.\n";
    }
  }while(choice < 0 || choice > (sizeof(Type)-1));

  switch(choice){
  case org:
    tempStr = TypeStr(org);    
    break;
  case pdf:
    tempStr = TypeStr(pdf);
    break;
  case html:
    tempStr = TypeStr(html);
    break;
  case babel:
    tempStr = TypeStr(babel);
    break;
  default:
    break;
  }

  PATH += tempStr;
  PATH += ".";
  PATH += TypeStr(org);

  Template.open(PATH);
  
  if(Template.is_open()){
    do{
      cout << "\t(1) -> Standard Details\n";
      cout << "\t(2) -> Custom Details\n";
      cout << "Enter Choice: "; 
      getline(cin, line);
      if(!isdigit(line[0])){
	cout << "Invalid Input.\n";
      }
      else{
	choice = stoi(line, &temp);
      }
      if(choice < 0 || choice > (sizeof(Type)-1)){
	cout << "Invalid Input.\n";
      }
    }while(choice < 0 || choice >2);

    cout << "\nTitle: ";
    getline(cin, title);
    cout << "Description: ";
    getline(cin, description);

    while(getline(Template, line)){
      temp = line.find("#+");
      if(temp != string::npos){
	if(temp == 0){
	  temp = line.find(":");
	  tempStr = line.substr(0,temp+1);
	  file << tempStr;
	  //tempInt = OrgVarInt(tempStr)
	  
	  if(tempStr == "#+title:"){
	    file << " " << title << endl;
	  }
	  else if(tempStr == "#+description:"){
	    file << " " << description << endl;
	  }
	  else if(tempStr == "#+date:"){
	    tempStr = GetOrgDate();
	    file << " " << tempStr << endl;
	  }
	  else if(tempStr == "#+LATEX_HEADER:"){
	    file << line.substr(temp+1) << endl;
	  }
	  // else if(choice == 2){
	  //   if(tempStr == "#+email:"){
	  //     cout << "Enter Email: ";
	  //     getline(cin, line);
	  //     file << " " << line << endl;
	  //   }
	  //   else{
	  //     file <<" " <<  line.substr(temp+1) << endl;
	  //   }
	  // }
	  // else if(tempStr == "#+options:"){
	  //   //file << line.substr(temp+1);
	  // }
	  else{ file << line.substr(temp+1) << endl;}
	}
	else{file << line << endl;}
      }
      else{file << line << endl;}
    }
  }
  
  cmd << "echo '" << file.str() <<"'| clipboard";
  tempStr = cmd.str();
  CMD = &(tempStr[0]);
  system(CMD);
  
  Template.close();
  cout << "Org Template copied to clipboard.\n";
  
  return 0;
}
