# org-gen makefile
org-gen: main.o fileType.o
	g++ main.o fileType.o -o org-gen
main.o: main.cpp
	g++ -c main.cpp
fileType.o: fileType.cpp fileType.h
	g++ -c fileType.cpp
move:
	cp -f org-gen ../
clean:
	rm *.o
