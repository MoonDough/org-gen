#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ctime>
#include <iomanip>

using namespace std;

string GetOrgDate(){
  ostringstream OrgDate;
  time_t rawtime = time(0);
  tm *local_time = localtime(&rawtime);
  string day, result;

  switch(local_time->tm_wday){
  case 0:
    day = "sun";
    break;
  case 1:
    day = "mon";
    break;
  case 2:
    day = "tue";
    break;
  case 3:
    day = "wed";
    break;
  case 4:
    day = "thu";
    break;
  case 5:
    day = "fri";
    break;
  default:
    day = "sat";
    break;
  }

  OrgDate << "<" << 1900 + local_time->tm_year << "-" << setfill('0') << setw(2)
	  << 1 + local_time->tm_mon << setw(1) << "-" << setw(2)
	  << local_time->tm_mday << " " << day << ">";
  result = OrgDate.str();
  return result;
}
